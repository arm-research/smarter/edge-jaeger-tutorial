package tracing

import (
	"fmt"
	"io"

	opentracing "github.com/opentracing/opentracing-go"
	jaeger "github.com/uber/jaeger-client-go"
	config "github.com/uber/jaeger-client-go/config"
)

// Init returns an instance of Jaeger Tracer that samples 100% of traces and logs all spans to stdout.
func Init(service string) (opentracing.Tracer, io.Closer) {

	// set environment variables
	jaegerAgentHost, set := os.LookupEnv("JAEGER_AGENT_HOST")
	if !set {
		jaegerAgentHost = "jaeger-agent"
	}
	jaegerAgentPort, set := os.LookupEnv("JAEGER_AGENT_PORT")
	if !set {
		jaeger_agent_port = "6831"
	}
	nodeName, set := os.LookupEnv("MY_NODE_NAME")
	if !set {
		panic("Please set env variable MY_NODE_NAME")
	}

	cfg := &config.Configuration{
		ServiceName: fmt.Sprintf("%s-%s", nodeName, service),
		Sampler: &config.SamplerConfig{
			Type:  "const",
			Param: 1,
		},
		Reporter: &config.ReporterConfig{
			LogSpans: true,
			LocalAgentHostPort: fmt.Sprintf("%s:%s", jaegerAgentHost, jaegerAgentPort)
		},
	}
	tracer, closer, err := cfg.New(service, config.Logger(jaeger.StdLogger))
	if err != nil {
		panic(fmt.Sprintf("ERROR: cannot init Jaeger: %v\n", err))
	}
	return tracer, closer
}
