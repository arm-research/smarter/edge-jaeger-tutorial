package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"

	opentracing "github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	jaeger "github.com/uber/jaeger-client-go"
	config "github.com/uber/jaeger-client-go/config"
)

func main() {
	tracer, closer := Init("publisher")
	defer closer.Close()

	http.HandleFunc("/publish", func(w http.ResponseWriter, r *http.Request) {
		spanCtx, _ := tracer.Extract(opentracing.HTTPHeaders, opentracing.HTTPHeadersCarrier(r.Header))
		span := tracer.StartSpan("publish", ext.RPCServerOption(spanCtx))
		defer span.Finish()

		helloStr := r.FormValue("helloStr")
		println(helloStr)
	})

	log.Fatal(http.ListenAndServe(":8082", nil))
}

// Init returns an instance of Jaeger Tracer that samples 100% of traces and logs all spans to stdout.
func Init(service string) (opentracing.Tracer, io.Closer) {

	// set environment variables
	jaegerAgentHost, set := os.LookupEnv("JAEGER_AGENT_HOST")
	if !set {
		jaegerAgentHost = "jaeger-agent"
	}
	jaegerAgentPort, set := os.LookupEnv("JAEGER_AGENT_PORT")
	if !set {
		jaegerAgentPort = "6831"
	}
	nodeName, set := os.LookupEnv("MY_NODE_NAME")
	if !set {
		panic("Please set env variable MY_NODE_NAME")
	}

	cfg := &config.Configuration{
		Sampler: &config.SamplerConfig{
			Type:  "const",
			Param: 1,
		},
		Reporter: &config.ReporterConfig{
			LogSpans:           true,
			LocalAgentHostPort: fmt.Sprintf("%s:%s", jaegerAgentHost, jaegerAgentPort),
		},
	}
	tracer, closer, err := cfg.New(fmt.Sprintf("%s-%s", nodeName, service), config.Logger(jaeger.StdLogger))
	if err != nil {
		panic(fmt.Sprintf("ERROR: cannot init Jaeger: %v\n", err))
	}
	return tracer, closer
}
