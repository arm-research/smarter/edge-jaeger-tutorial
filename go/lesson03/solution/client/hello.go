package main

import (
	"context"
	"fmt"
	"io"
	"io/ioutil"
	mylog "log"
	"net/http"
	"net/url"
	"os"

	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	"github.com/opentracing/opentracing-go/log"
	jaeger "github.com/uber/jaeger-client-go"
	config "github.com/uber/jaeger-client-go/config"
)

func main() {
	tracer, closer := Init("hello-world")
	defer closer.Close()
	opentracing.SetGlobalTracer(tracer)

	http.HandleFunc("/hello", func(w http.ResponseWriter, r *http.Request) {
		helloTo := r.FormValue("helloTo")

		span := tracer.StartSpan("say-hello")
		span.SetTag("hello-to", helloTo)
		defer span.Finish()

		ctx := opentracing.ContextWithSpan(context.Background(), span)

		helloStr := formatString(ctx, helloTo)
		printHello(ctx, helloStr)

		w.Write([]byte(helloStr))
	})

	mylog.Fatal(http.ListenAndServe(":8080", nil))
}

func formatString(ctx context.Context, helloTo string) string {
	span, _ := opentracing.StartSpanFromContext(ctx, "formatString")
	defer span.Finish()

	v := url.Values{}
	v.Set("helloTo", helloTo)
	url := "http://formatter:8081/format?" + v.Encode()
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		panic(err.Error())
	}

	ext.SpanKindRPCClient.Set(span)
	ext.HTTPUrl.Set(span, url)
	ext.HTTPMethod.Set(span, "GET")
	span.Tracer().Inject(
		span.Context(),
		opentracing.HTTPHeaders,
		opentracing.HTTPHeadersCarrier(req.Header),
	)

	resp, err := Do(req)
	if err != nil {
		panic(err.Error())
	}

	helloStr := string(resp)

	span.LogFields(
		log.String("event", "string-format"),
		log.String("value", helloStr),
	)

	return helloStr
}

func printHello(ctx context.Context, helloStr string) {
	span, _ := opentracing.StartSpanFromContext(ctx, "printHello")
	defer span.Finish()

	v := url.Values{}
	v.Set("helloStr", helloStr)
	url := "http://publisher:8082/publish?" + v.Encode()
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		panic(err.Error())
	}

	ext.SpanKindRPCClient.Set(span)
	ext.HTTPUrl.Set(span, url)
	ext.HTTPMethod.Set(span, "GET")
	span.Tracer().Inject(span.Context(), opentracing.HTTPHeaders, opentracing.HTTPHeadersCarrier(req.Header))

	if _, err := Do(req); err != nil {
		panic(err.Error())
	}
}

// Init returns an instance of Jaeger Tracer that samples 100% of traces and logs all spans to stdout.
func Init(service string) (opentracing.Tracer, io.Closer) {

	// set environment variables
	jaegerAgentHost, set := os.LookupEnv("JAEGER_AGENT_HOST")
	if !set {
		jaegerAgentHost = "jaeger-agent"
	}
	jaegerAgentPort, set := os.LookupEnv("JAEGER_AGENT_PORT")
	if !set {
		jaegerAgentPort = "6831"
	}
	nodeName, set := os.LookupEnv("MY_NODE_NAME")
	if !set {
		panic("Please set env variable MY_NODE_NAME")
	}

	cfg := &config.Configuration{
		Sampler: &config.SamplerConfig{
			Type:  "const",
			Param: 1,
		},
		Reporter: &config.ReporterConfig{
			LogSpans:           true,
			LocalAgentHostPort: fmt.Sprintf("%s:%s", jaegerAgentHost, jaegerAgentPort),
		},
	}
	tracer, closer, err := cfg.New(fmt.Sprintf("%s-%s", nodeName, service), config.Logger(jaeger.StdLogger))
	if err != nil {
		panic(fmt.Sprintf("ERROR: cannot init Jaeger: %v\n", err))
	}
	return tracer, closer
}

// Do executes an HTTP request and returns the response body.
// Any errors or non-200 status code result in an error.
func Do(req *http.Request) ([]byte, error) {
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("StatusCode: %d, Body: %s", resp.StatusCode, body)
	}

	return body, nil
}
